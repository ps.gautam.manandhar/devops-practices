CREATE OR REPLACE PACKAGE GAUTAM 
AS
    PROCEDURE P_PRINT_HELLO(name in VARCHAR2);
    PROCEDURE P_CREATE_TAB_DEPT;
    PROCEDURE P_CREATE_TAB_EMP;
    PROCEDURE P_INSERT_DATA;
    PROCEDURE P_PRINT_EMP_SAL;
    PROCEDURE P_UPDATE_SAL(emp_no NUMBER, new_salary NUMBER);
    PROCEDURE P_SQL_FUN;
    PROCEDURE P_DELETE_USER (dept_id NUMBER);
    FUNCTION F_DEPT10_HR(dept NUMBER, salary NUMBER) RETURN VARCHAR2;
    FUNCTION F_EMP_HR RETURN VARCHAR2;
    FUNCTION F_HR_Supervisor RETURN SYS_REFCURSOR;
    FUNCTION F_HR_REPORT RETURN SYS_REFCURSOR;
    FUNCTION F_HR_MANAGER RETURN SYS_REFCURSOR;
    FUNCTION F_HR_QUERY RETURN SYS_REFCURSOR;
    FUNCTION F_SAL_ROUND RETURN SYS_REFCURSOR;
   
    
end GAUTAM;
/

CREATE OR REPLACE PACKAGE BODY GAUTAM 
AS
    --Procedure 1
    PROCEDURE P_PRINT_HELLO (name VARCHAR2)
    IS
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Hello,'|| name);
    END P_PRINT_HELLO;
    
    --Procedure 2
    PROCEDURE P_CREATE_TAB_DEPT IS
    BEGIN
        EXECUTE IMMEDIATE '
            CREATE TABLE DEPT (
                DEPTNO NUMBER(2) NOT NULL PRIMARY KEY,
                DNAME VARCHAR2(14),
                LOC VARCHAR2(13) 
                   
            )
        ';
    END P_CREATE_TAB_DEPT;
    
    --Procedure 3
    PROCEDURE P_CREATE_TAB_EMP 
    IS
    BEGIN
        EXECUTE IMMEDIATE '
             CREATE TABLE EMP (
                EMPNO NUMBER(4) NOT NULL PRIMARY KEY,
                ENAME VARCHAR2(10),
                JOB VARCHAR2(9),
                MGR NUMBER(4),
                HIREDATE DATE,
                SAL NUMBER(7,2),
                COMM NUMBER(7,2),
                DEPTNO NUMBER(2) NOT NULL,
                CONSTRAINT FK_DEPTNO_EMP FOREIGN KEY (DEPTNO) REFERENCES DEPT (DEPTNO)
            )
        ';
    END P_CREATE_TAB_EMP;
    
    
     --Procedure 4   
    PROCEDURE P_INSERT_DATA IS
    BEGIN
        INSERT INTO DEPT VALUES (10, 'ACCOUNTING', 'NEW YORK');
        INSERT INTO DEPT VALUES (20, 'RESEARCH', 'DALLAS');
        INSERT INTO DEPT VALUES (30, 'SALES', 'CHICAGO');
        INSERT INTO DEPT VALUES (40, 'OPERATIONS', 'BOSTON');
        INSERT INTO EMP  VALUES (7369, 'SMITH', 'CLERK', 7902, TO_DATE('17-DEC-80', 'DD-MON-YY'), 800, NULL, 20);
        INSERT INTO EMP  VALUES (7499, 'ALLEN', 'SALESMAN', 7698, TO_DATE('20-FEB-81', 'DD-MON-YY'), 1600, 300, 30);
        INSERT INTO EMP  VALUES (7521, 'WARD', 'SALESMAN', 7698, TO_DATE('22-FEB-81', 'DD-MON-YY'), 1250, 500, 30);
        INSERT INTO EMP  VALUES (7566, 'JONES', 'MANAGER', 7839, TO_DATE('02-APR-81', 'DD-MON-YY'), 2975, NULL, 20);
        INSERT INTO EMP  VALUES (7654, 'MARTIN', 'SALESMAN', 7698, TO_DATE('28-SEP-81', 'DD-MON-YY'), 1250, 1400, 30);
        INSERT INTO EMP  VALUES (7698, 'BLAKE', 'MANAGER', 7839, TO_DATE('01-MAY-81', 'DD-MON-YY'), 2850, NULL, 30);
        INSERT INTO EMP  VALUES (7782, 'CLARK', 'MANAGER', 7839, TO_DATE('09-JUN-81', 'DD-MON-YY'), 2450, NULL, 10);
        INSERT INTO EMP  VALUES (7788, 'SCOTT', 'ANALYST', 7566, TO_DATE('19-APR-87', 'DD-MON-YY'), 3000, NULL, 20);
        INSERT INTO EMP  VALUES (7839, 'KING', 'PRESIDENT', NULL, TO_DATE('17-NOV-81', 'DD-MON-YY'), 5000, NULL, 10);
        INSERT INTO EMP  VALUES (7844, 'TURNER', 'SALESMAN', 7698, TO_DATE('08-SEP-81', 'DD-MON-YY'), 1500, 0, 30);
        INSERT INTO EMP  VALUES (7876, 'ADAMS', 'CLERK', 7788, TO_DATE('23-MAY-87', 'DD-MON-YY'), 1100, NULL, 20);
        INSERT INTO EMP  VALUES (7900, 'JAMES', 'CLERK', 7698, TO_DATE('03-DEC-81', 'DD-MON-YY'), 950, NULL, 30);
        INSERT INTO EMP  VALUES (7902, 'FORD', 'ANALYST', 7566, TO_DATE('03-DEC-81', 'DD-MON-YY'), 3000, NULL, 20);
        INSERT INTO EMP  VALUES (7934, 'MILLER', 'CLERK', 7782, TO_DATE('23-JAN-82', 'DD-MON-YY'), 1300, NULL, 10);
        
    END P_INSERT_DATA;
    
    
    --Procedure 5
    PROCEDURE P_PRINT_EMP_SAL 
    IS
    BEGIN
        FOR emp_rec IN (
            SELECT ENAME, DNAME, SAL
            FROM EMP e
            JOIN DEPT d ON e.DEPTNO = d.DEPTNO
        )
        LOOP
            DBMS_OUTPUT.PUT_LINE('Salary for Employee ' || emp_rec.ENAME || ' from Department ' || emp_rec.DNAME || ' = ' || emp_rec.SAL || '!');
        END LOOP;
    END P_PRINT_EMP_SAL;
    
    --Procedure 6
    PROCEDURE P_UPDATE_SAL (emp_no IN NUMBER, new_salary IN NUMBER) IS
    BEGIN
        UPDATE EMP
        SET SAL = new_salary
        WHERE EMPNO = emp_no;
        DBMS_OUTPUT.PUT_LINE('Salary updated for employee number ' || emp_no || ' and the new salary is ' || new_salary);
    END P_UPDATE_SAL;
    
    
    --Procedure 7
    PROCEDURE P_SQL_FUN
    IS
    BEGIN
        FOR emp_rec IN (
            SELECT ENAME, RPAD('*', ROUND(SAL/100), '*') AS SALARY
            FROM EMP
            ORDER BY SAL DESC
        ) LOOP
        DBMS_OUTPUT.PUT_LINE(emp_rec.ENAME || ' ' || emp_rec.SALARY);
    END LOOP;
    END P_SQL_FUN;
    
    
    --Procedure 8
    PROCEDURE P_DELETE_USER (dept_id in NUMBER) 
    IS
    BEGIN
        DELETE FROM DEPT WHERE DEPTNO = dept_id;
        COMMIT;
    END P_DELETE_USER;
    
   


    -- function 2
    FUNCTION F_DEPT10_HR(dept NUMBER, salary NUMBER) RETURN VARCHAR2 IS 
        response VARCHAR2(244);  -- Variable to store the response string
    BEGIN
        FOR EMPLOYEE IN (SELECT EMPNO, ENAME, SAL, DEPTNO  FROM EMP  WHERE DEPTNO = dept AND SAL > salary) LOOP
            response:= EMPLOYEE.EMPNO||' '|| EMPLOYEE.ENAME||' '|| EMPLOYEE.SAL||' '|| EMPLOYEE.DEPTNO;
            dbms_output.put_line(response);  -- Output the employee details to the console
        END LOOP;
      --RETURN 'Process complete';  -- Return NULL as the function does not return any meaningful value
        RETURN NULL;
    END F_DEPT10_HR;
    
         
     -- function 3
    FUNCTION F_EMP_HR RETURN VARCHAR2 IS 
        response VARCHAR2(244);  -- Variable to store the response string
    BEGIN
        FOR EMPLOYEE IN (SELECT * FROM EMP WHERE SAL NOT BETWEEN 4000 and 6000) LOOP
            response:= EMPLOYEE.EMPNO||' '|| EMPLOYEE.ENAME||' '||EMPLOYEE.JOB||' '||EMPLOYEE.MGR||' '||EMPLOYEE.HIREDATE||' '||EMPLOYEE.SAL||' '||EMPLOYEE.COMM||' '||EMPLOYEE.DEPTNO;
            dbms_output.put_line(response);  -- Output the employee details to the console
        END LOOP;
      RETURN NULL;  -- Return NULL as the function does not return any meaningful value
    END F_EMP_HR;
    
    
    
    -- function 4
    FUNCTION F_HR_Supervisor RETURN SYS_REFCURSOR IS
    supervisor_cursor SYS_REFCURSOR;
    BEGIN
        OPEN supervisor_cursor FOR
            SELECT mgr AS Supervisor_ID, COUNT(empno) AS Num_Supervisees, MAX(sal) AS Highest_Salary
            FROM emp
            GROUP BY mgr;
        RETURN supervisor_cursor;
    END F_HR_Supervisor;
    
    
    -- function 5
    FUNCTION F_HR_REPORT RETURN SYS_REFCURSOR IS
        report_cursor SYS_REFCURSOR;
        hire_date DATE;
        review_date DATE;
        first_friday DATE;
        last_day_of_month DATE;
    BEGIN
    OPEN report_cursor FOR
        SELECT 
            EMPNO,
            HIREDATE,
            ROUND(MONTHS_BETWEEN(HIREDATE, SYSDATE)) AS Months_Employed,
            ADD_MONTHS(HIREDATE, 6) AS Six_Month_Review_Date,
            NEXT_DAY(HIREDATE, 'FRIDAY') AS First_Friday_After_Hire_Date,
            LAST_DAY(HIREDATE) AS Last_Day_Of_Month_When_Hired
        FROM EMP
        WHERE MONTHS_BETWEEN(SYSDATE, HIREDATE) < 200;
    RETURN report_cursor;
    END F_HR_REPORT;
    
    
    -- function 6
    FUNCTION F_HR_MANAGER RETURN SYS_REFCURSOR IS
        emp_cursor SYS_REFCURSOR;
    BEGIN
        OPEN emp_cursor FOR
            SELECT e1.ENAME AS Employee, e1.EMPNO AS Emp#, e2.ENAME AS Manager,e2.EMPNO AS Mgr#
            FROM EMP e1
            LEFT JOIN EMP e2 ON e1.MGR = e2.EMPNO;
        RETURN emp_cursor;
    END F_HR_MANAGER;
    
    
    --function 7
    
    FUNCTION F_HR_QUERY RETURN SYS_REFCURSOR
    IS
        emp_cursor SYS_REFCURSOR;
    BEGIN
        OPEN emp_cursor FOR
            SELECT ENAME, HIREDATE
            FROM EMP
            WHERE HIREDATE > (SELECT HIREDATE FROM EMP WHERE ENAME = 'JONES');
        RETURN emp_cursor;
    END F_HR_QUERY;
    
    
    
    -- function 8 
  
   FUNCTION F_SAL_ROUND RETURN SYS_REFCURSOR
    IS
        emp_cursor SYS_REFCURSOR;  --declare a cursor emp_cursor of type SYS_REFCURSOR
    BEGIN
        OPEN emp_cursor FOR  --opening the cursor for a query
            SELECT ENAME, ROUND(SAL, -3) AS Rounded_salary FROM EMP;  --This statement opens the cursor emp_cursor for a query that selects the employee names and their salaries rounded to the nearest thousandth.
        RETURN emp_cursor;
    END F_SAL_ROUND;


  
END GAUTAM;
/

#############################################################Testing the above procedures and functions##########################################################################

set serveroutput on;

EXEC gautam.P_PRINT_HELLO('Gautam Manandhar');
EXEC gautam.P_CREATE_TAB_DEP;
EXEC gautam.P_CREATE_TAB_EMP;
EXEC gautam.P_INSERT_DATA;
EXEC gautam.P_PRINT_EMP_SAL;
EXEC gautam.P_UPDATE_SAL(7900, 1900);
EXEC gautam.P_SQL_FUN;
EXEC gautam.P_DELETE_USER(40);

--function 2
BEGIN
    DBMS_OUTPUT.PUT_LINE(gautam.F_DEPT10_HR(10, 4000));
END;
/

--function 3
BEGIN
    DBMS_OUTPUT.PUT_LINE(gautam.F_EMP_HR());
END;
/

--function 4
VAR rc REFCURSOR;
EXEC :rc := gautam.F_HR_Supervisor;
PRINT rc;

--function 5
VAR rc REFCURSOR;
EXEC :rc := gautam.F_HR_REPORT;
PRINT rc;

--function 6
VAR rc REFCURSOR;
EXEC :rc := gautam.F_HR_MANAGER;
PRINT rc;

--function 7
VAR rc REFCURSOR;
EXEC :rc := gautam.F_HR_QUERY;
PRINT rc;

--function 8
VAR rc REFCURSOR;
EXEC :rc := gautam.F_SAL_ROUND;
PRINT rc;


#######################################################################DATABASE LINK##################################################################################################

Create database link to select all department from your colleague database.

Open the tnsnames.ora file located in the $ORACLE_HOME/network/admin directory on your Oracle database server.


colleague_db =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = colleague_hostname)(PORT = colleague_port))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = colleague_service_name)
    )
  )
Replace the placeholders (colleague_hostname, colleague_port, and colleague_service_name) with the actual values for your colleague's database.

Save the tnsnames.ora file.


CREATE DATABASE LINK colleague_db
CONNECT TO colleague_username IDENTIFIED BY colleague_password
USING 'colleague_db';


SELECT * FROM departments@colleague_db;


##########################################################################MATERIALIZED VIEW######################################################################################

Write Materialized view to include:
Create a report for the HR department that displays employee last names, department numbers, and all the employees who work in the same department as a given employee. Give each column an
appropriate label

CREATE MATERIALIZED VIEW mv_hr_department_report
AS
SELECT d.DEPTNO AS DEPARTMENT,
       e1.ENAME AS EMPLOYEE,
       e2.ENAME AS COLLEAGUE
FROM EMP e1
JOIN DEPT d ON e1.DEPTNO = d.DEPTNO
JOIN EMP e2 ON e1.DEPTNO = e2.DEPTNO
WHERE e1.EMPNO != e2.EMPNO
ORDER BY d.DEPTNO, e1.ENAME, e2.ENAME;

-- Query to view the materialized view
SELECT * FROM mv_hr_department_report;


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Create schedule Job to refresh Above Materialized view every 5 minute except Friday & Saturday.

BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
    job_name        => 'REFRESH_MV_HR_DEPT_REPORT',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN DBMS_MVIEW.REFRESH("REFRESH_MV_HR_DEPT_REPORT"); END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=MINUTELY;INTERVAL=5;BYDAY=MON,TUE,WED,THU,SUN',
    enabled         => TRUE
  );
END;
/

SELECT * FROM DBA_SCHEDULER_JOBS WHERE JOB_NAME = 'REFRESH_MV_HR_DEPT_REPORT'


############################################################################TRIGGERS#############################################################################################

CREATE TABLE ps_orders( id number(5), quantity number(4), cost_per_item number(6,2), total_cost number(8,2), create_date date, created_by varchar2(10),update_date date,updated_by VARCHAR2(10));

Create a trigger to insert auto value for the ID column ( for each insert must increase by 1).


CREATE SEQUENCE id_seq START WITH 1 INCREMENT BY 1;
 
This sequence (id_seq) will start at 1 and increment by 1 for each new value.


CREATE OR REPLACE TRIGGER trg_insert_auto_id
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
    SELECT id_seq.NEXTVAL INTO :new.ID FROM dual;
END;



SELECT sequence_name, increment_by FROM user_sequences WHERE sequence_name = 'id_seq';

select * from ps_orders;
BEGIN
    INSERT INTO ps_orders (id, quantity, cost_per_item, total_cost, create_date, created_by, update_date, updated_by)
    VALUES (1, 50, 99.50, 5000.00, TO_DATE('2024-05-20', 'YYYY-MM-DD'), 'Gautam', TO_DATE('2024-05-20', 'YYYY-MM-DD'), 'Gautam');
END;
/
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Create a trigger to set create date column with the current date and time.

CREATE OR REPLACE TRIGGER trg_create_date
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
    :NEW.create_date:= SYSDATE;
END;
/

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Create a trigger to set created by column with the current user who makes the insert.

CREATE OR REPLACE TRIGGER trg_insert
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
    :NEW.created_by:= USER;
END;
/

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Create a trigger to calculate the total cost column by multiple quantities with cost per item.

CREATE OR REPLACE TRIGGER trg_calc_total_cost
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
    :NEW.total_cost:= :NEW.quantity* :NEW.cost_per_item;
END;
/

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Create a trigger to update (update date and update by) columns if the user executes the update statement on the table.

CREATE OR REPLACE TRIGGER trg_update
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
    :NEW.update_date:= SYSTIMESTAMP;
    :NEW.updated_by:= USER;
    
END;
/


##########################################################################INDEX#######################################################################################################

CREATE TABLE p_info (
    id NUMBER PRIMARY KEY,
    name VARCHAR2(30),
    age NUMBER,
    department VARCHAR2(20)
);

-- B-Tree index on ID column
CREATE INDEX idx_btree_id ON p_info(id);

-- Bitmap index on AGE column
CREATE BITMAP INDEX idx_bitmap_age ON p_info(age);

-- Unique index on NAME column
CREATE UNIQUE INDEX idx_unique_name ON p_info(name);

-- Function-based index on AGE column using UPPER function
CREATE INDEX idx_func_based_upper ON p_info(UPPER(name));

-- Concatenated index on NAME and DEPARTMENT columns
CREATE INDEX idx_concat_name_dept ON p_info(name, department);
