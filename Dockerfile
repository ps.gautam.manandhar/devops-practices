# Use AdoptOpenJDK's Alpine image with JRE 11 as the base image
FROM adoptopenjdk/openjdk11:alpine-jre

# Define a build argument 'hash' and set it as an environment variable 'HASH'
ARG hash
ENV HASH=$hash

# Copy your application artifact into the container
COPY target/assignment-$HASH.jar /app/

# Set the working directory inside the container
WORKDIR /app

# Add a user 
RUN adduser -D devops

#Change the user from root to other user
USER devops

# Create an environment variable to pass the h2 profile variable
ENV H2_PROFILE=$H2_PROFILE

# Run the command to run the application
CMD java -jar $H2_PROFILE assignment-$HASH.jar


